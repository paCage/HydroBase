#ifndef _HYDRO_FLUX_H_
#define _HYDRO_FLUX_H_


#ifdef __cplusplus
extern "C" {
#endif


#define HYDRO_FLUX_T_LEN (6)


/* Flux type */
typedef union _hydro_flux_t {
  struct {
    double rho, mx, my, mz, e_tot, e_int;
  };

  double vars[HYDRO_FLUX_T_LEN];
} hydro_flux_t;

/* Flux variables indices */
enum hydro_flux_indices {
  rho_hf = 0,
  mx_hf = 1,
  my_hf = 2,
  mz_hf = 3,
  e_tot_hf = 4,
  e_int_hf = 5
};

/* Flux type (Structure of Arrays) */
typedef struct {
  int len;
  double *rho;
  double *mx;
  double *my;
  double *mz;
  double *e_tot;
  double *e_int;
} hydro_flux_SoA_t;


#ifdef __cplusplus
}
#endif


#endif /* _HYDRO_FLUX_H_ */
