#ifndef _COPY_1D_HYDRO_STATE_H_
#define _COPY_1D_HYDRO_STATE_H_


#include "./hydro.h"


#ifdef __cplusplus
extern "C" {
#endif

void copy_1d_hydro_state(hydro_t *target, hydro_t *source, int v_idx);

#ifdef __cplusplus
}
#endif


#endif /* _COPY_1D_HYDRO_STATE_H_ */
