#ifndef _SET_1D_HYDRO_STATE_H_
#define _SET_1D_HYDRO_STATE_H_


#include "./hydro.h"


#ifdef __cplusplus
extern "C" {
#endif

void set_1d_hydro_state(hydro_t *target, double rho, int v_idx, double v, double p);

#ifdef __cplusplus
}
#endif


#endif /* _SET_1D_HYDRO_STATE_H_ */
