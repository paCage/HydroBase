/*
 * allocate_hydro_SoA.c
 * tests_file: allocate_hydro_SoA_tests.c
 *
 * Allocating a structure of array of hydro variables with a given length
 *
 * @param: h: A pointer to a given hydro structure
 * @param: len: The length of arrays
 * @param: allocate: A pointer to the allocation function
 *
 * @return: void
 */


#include <stdlib.h>
#include "./allocate_hydro_SoA.h"


void allocate_hydro_SoA(hydro_SoA_t *h, int len, void allocate(int, size_t, void **))
{
  allocate(len, sizeof(double), (void **)&(h->rho));
  allocate(len, sizeof(double), (void **)&(h->vx));
  allocate(len, sizeof(double), (void **)&(h->vy));
  allocate(len, sizeof(double), (void **)&(h->vz));
  allocate(len, sizeof(double), (void **)&(h->p));
  h->len = len;
}
