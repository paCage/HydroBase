#ifndef _HYDRO_H_
#define _HYDRO_H_


#ifdef __cplusplus
extern "C" {
#endif


#define HYDRO_T_LEN (5)


/* Hydro type */
typedef union _hydro_t {
  struct {
    double rho, u, v, w, p;
  };

  double vars[HYDRO_T_LEN];
} hydro_t;

/* Hydro vars indices */
enum hydro_indices {
  rho_h = 0,
  vx_h = 1,
  vy_h = 2,
  vz_h = 3,
  p_h = 4
};

/* Hydro type (structure of arrays) */
typedef struct {
  int len;
  double *rho;
  double *vx;
  double *vy;
  double *vz;
  double *p;
} hydro_SoA_t;


#ifdef __cplusplus
}
#endif


#endif /* _HYDRO_H_ */
