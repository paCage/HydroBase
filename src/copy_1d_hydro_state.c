/*
 * copy_1d_hydro_state.c
 * tests_file: copy_1d_hydro_state_tests.c
 *
 * Copying 1 dimensional hydro state
 *
 * @param: target: Target hydro state to be filled
 * @param: source: Source hydro state to be copied
 *
 * @return: void
 */


#include "./copy_1d_hydro_state.h"


void copy_1d_hydro_state(hydro_t *target, hydro_t *source, int v_idx)
{
  target->rho = source->rho;
  target->vars[v_idx] = source->vars[v_idx];
  target->p = source->p;
}
