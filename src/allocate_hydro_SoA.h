#ifndef _ALLOCATE_HYDRO_SOA_H_
#define _ALLOCATE_HYDRO_SOA_H_


#include <stdlib.h>
#include "./hydro.h"


#ifdef __cplusplus
extern "C" {
#endif

void allocate_hydro_SoA(hydro_SoA_t *, int, void allocate(int, size_t, void **));

#ifdef __cplusplus
}
#endif


#endif /* _ALLOCATE_HYDRO_SOA_H_ */
