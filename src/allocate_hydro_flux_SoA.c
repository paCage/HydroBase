/*
 * allocate_flux_SoA.c
 * tests_file: allocate_flux_SoA_tests.c
 *
 * Allocating a structure of array of fluxes variables with a given length
 *
 * @param: h: A pointer to a given flux structure
 * @param: len: The length of arrays
 * @param: allocate: A pointer to the allocation function
 *
 * @return: void
 */


#include <stdlib.h>
#include "./allocate_hydro_flux_SoA.h"


void allocate_hydro_flux_SoA(hydro_flux_SoA_t *h, int len, void allocate(int, size_t, void **))
{
  allocate(len, sizeof(double), (void **)&(h->rho));
  allocate(len, sizeof(double), (void **)&(h->mx));
  allocate(len, sizeof(double), (void **)&(h->my));
  allocate(len, sizeof(double), (void **)&(h->mz));
  allocate(len, sizeof(double), (void **)&(h->e_tot));
  allocate(len, sizeof(double), (void **)&(h->e_int));
  h->len = len;
}
