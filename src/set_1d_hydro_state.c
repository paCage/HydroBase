/*
 * set_1d_hydro_state.c
 * tests_file: set_1d_hydro_state_tests.c
 *
 * Setting 1 dimensional hydro state
 *
 * @param: target: Target hydro state to be filled
 * @param: rho: Density
 * @param: v_idx: Index of the velocity to be filled
 * @param: v: Velocity
 * @param: p: Pressure
 *
 * @return: void
 */


#include "./set_1d_hydro_state.h"


void set_1d_hydro_state(hydro_t *target, double rho, int v_idx, double v, double p)
{
  target->rho = rho;
  target->vars[v_idx] = v;
  target->p = p;
}
