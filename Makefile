MODULE_NAME := HydroBase

MAJOR := 0
MINOR := 0
PATCH := 0


OBJS := allocate_hydro_SoA.o \
        allocate_hydro_flux_SoA.o \
        set_1d_hydro_state.o \
        copy_1d_hydro_state.o \


TEST_OBJS := allocate_hydro_SoA_tests.o \
             allocate_hydro_flux_SoA_tests.o \
             set_1d_hydro_state_tests.o \
             copy_1d_hydro_state_tests.o \
             hydro_flux_tests.o \
             hydro_tests.o \


LIBS :=
paCages :=


include ./Makefile.paCage/Makefile
