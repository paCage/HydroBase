/*
 * copy_1d_hydro_state_tests.c
 */


#include <cgreen/cgreen.h>
#include "./../src/copy_1d_hydro_state.h"


Describe(copy_1d_hydro_state);
BeforeEach(copy_1d_hydro_state) {};
AfterEach(copy_1d_hydro_state) {};


hydro_t target;
hydro_t source = {
  .rho = 1.23,
  .u = 2.34,
  .v = 3.45, 
  .w = 4.56,
  .p = 5.67
};


Ensure(copy_1d_hydro_state, works_properly)
{
  target.v = 6.78;
  target.w = 7.89;

  copy_1d_hydro_state(&target, &source, vx_h);


  assert_that_double(target.rho, is_equal_to_double(1.23));
  assert_that_double(target.u, is_equal_to_double(2.34));
  assert_that_double(target.vars[vx_h], is_equal_to_double(2.34));
  assert_that_double(target.p, is_equal_to_double(5.67));

  assert_that_double(target.v, is_equal_to_double(6.78));
  assert_that_double(target.w, is_equal_to_double(7.89));
}
