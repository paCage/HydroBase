/*
 * allocate_hydro_SoA_tests.c
 */


#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>
#include "./../src/allocate_hydro_SoA.h"

#define LEN 13

void mocked_allocate_h(int len, size_t size, void **p) {
    mock(len, size, p);
}


Describe(allocate_hydro_SoA);
BeforeEach(allocate_hydro_SoA) {};
AfterEach(allocate_hydro_SoA) {};


Ensure(allocate_hydro_SoA, calls_the_allocation_function_and_sets_the_length)
{
  hydro_SoA_t h;

  expect(mocked_allocate_h, when(len, is_equal_to(LEN)));
  expect(mocked_allocate_h, when(len, is_equal_to(LEN)));
  expect(mocked_allocate_h, when(len, is_equal_to(LEN)));
  expect(mocked_allocate_h, when(len, is_equal_to(LEN)));
  expect(mocked_allocate_h, when(len, is_equal_to(LEN)));

  allocate_hydro_SoA(&h, LEN, mocked_allocate_h);

  assert_that(h.len, is_equal_to(LEN));
}
