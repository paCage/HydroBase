/*
 * set_1d_hydro_state_tests.c
 */


#include <cgreen/cgreen.h>
#include "./../src/set_1d_hydro_state.h"


Describe(set_1d_hydro_state);
BeforeEach(set_1d_hydro_state) {};
AfterEach(set_1d_hydro_state) {};


hydro_t target;


Ensure(set_1d_hydro_state, works_properly)
{
  target.vars[vy_h] = 4.56;
  target.vars[vz_h] = 5.67;
  set_1d_hydro_state(&target, 1.23, vx_h, 2.34, 3.45);

  assert_that_double(target.rho, is_equal_to_double(1.23));
  assert_that_double(target.vars[vx_h], is_equal_to_double(2.34));
  assert_that_double(target.p, is_equal_to_double(3.45));

  assert_that_double(target.vars[vy_h], is_equal_to_double(4.56));
  assert_that_double(target.vars[vz_h], is_equal_to_double(5.67));
}
