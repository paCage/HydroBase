/*
 * hydro_flux_tests.c
 */


#include <cgreen/cgreen.h>
#include "./../src/hydro_flux.h"


Describe(hydro_flux);
BeforeEach(hydro_flux) {};
AfterEach(hydro_flux) {};


Ensure(hydro_flux, has_correct_data_structure)
{
  hydro_flux_t hf;

  hf.rho = 1.23;
  assert_that_double(hf.rho, is_equal_to_double(1.23));

  hf.mx = 2.34;
  assert_that_double(hf.mx, is_equal_to_double(2.34));

  hf.my = 3.45;
  assert_that_double(hf.my, is_equal_to_double(3.45));

  hf.mz = 4.56;
  assert_that_double(hf.mz, is_equal_to_double(4.56));

  hf.e_tot = 5.67;
  assert_that_double(hf.e_tot, is_equal_to_double(5.67));

  hf.e_int = 6.78;
  assert_that_double(hf.e_int, is_equal_to_double(6.78));
}
