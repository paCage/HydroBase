/*
 * hydro_tests.c
 */


#include <cgreen/cgreen.h>
#include "./../src/hydro.h"


Describe(hydro);
BeforeEach(hydro) {};
AfterEach(hydro) {};


Ensure(hydro, has_correct_definitions)
{
}


Ensure(hydro, has_correct_data_structure)
{
  hydro_t h;

  h.rho = 1.23;
  assert_that_double(h.rho, is_equal_to_double(1.23));

  h.u = 2.34;
  assert_that_double(h.u, is_equal_to_double(2.34));

  h.v = 3.45;
  assert_that_double(h.v, is_equal_to_double(3.45));

  h.w = 4.56;
  assert_that_double(h.w, is_equal_to_double(4.56));

  h.p = 5.67;
  assert_that_double(h.p, is_equal_to_double(5.67));
}
